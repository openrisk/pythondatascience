## About Python Data Science


### How Does it Work?

Each Python Task View document lives in a separate Markdown file (or group of files within a directory) hosted in this [GitLab Repository](https://gitlab.com/openrisk/pythondatascience). 

The Markdown files are automatically processed and displayed in a [Gitlab Pages website](https://www.pythondatascience.org/)

### Authors

Anybody who has demonstrably good knowledge of Python Data Science tools used in any specific domain is welcome to contribute to the knowledge base. 

Similar to CRAN for the R ecosystem, we want to have a small number of authors per topic to help organize the content and ensure it is a high quality resource that adds value to all users. 


### Definitional Challenges

The term Data Science can be slippery. We lay out what it means for the PDF project in this [document](DataScienceDefinition.md)