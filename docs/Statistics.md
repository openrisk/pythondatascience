## Statistics

Python Packages for general statistics (may eventually split into subdomains as per corresponding
R task views). Currently only [Regression](Regression.md) is split out.


| Functionality | PyPI Link   | Description | Other       |
| ------------- | ----------- | ----------- | ----------- |
| Placeholder   | Placeholder | Placeholder | Placeholder |


