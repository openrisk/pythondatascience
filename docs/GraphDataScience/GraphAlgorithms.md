## Graph Algorithms

Graph Algorithms is a subdomain of graph concepts and tools that are used in more specialized types of [graph analysis](GraphAnalysis.md). The boundary is not clean.

### Search

#### Graph Traversal

* Breadth-first traversal
* Depth-first traversal 
* Dijkstra traversal
* Bellman-Ford traversal

#### Pathfinding and Cuts

#### Graph Comparisons / Morphisms

* Jaccard adjacency similarity
* Similarity between pairs of vertices
* Check whether two graphs are isomorphic
* Obtain all subgraph isomorphisms 


#### Spanning Trees

* minimum spanning tree of a given graph


| Functionality | PyPI / Repo Link | Description | Other |
|---------------|------------------|-------------|-------|


| Community Detection | [cdlib](https://github.com/GiulioRossetti/cdlib) | Python community detection library, with 60+ methods and evaluation/visualization features.  | CDlib is designed around the networkx python library, however, when needed, it takes care to automatically convert (from and to) igraph |



### Graph Algebra / Spectral Analysis

* Adjacency
* Laplacian
* Incidence
* Transition
* Modularity
* Hashimoto



python-louvain - A solid implementation of Louvain community detection algorithm.

scipy.sparse.csgraph - Fast graph algorithms based on sparse matrix representations.

SnapVX - A convex optimization solver for problems defined on a graph.