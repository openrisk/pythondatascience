## Graph Generation

Graph Generation can be decomposed naturally along two sub-categories: Deterministic or Random, depending on the primary mechanism the underlies the graph data generation.

* From node and edge lists provided in some format
* From adjacency or incidence matrices
* From dataframes
* From formulas


* Complete Graphs
* Tree and Star Graphs
* Lattice Graphs
* Circular Graphs

Generate a random graph, with a given degree distribution and (optionally) vertex-vertex correlation.

Shuffle the graph in-place, following a variety of possible statistical models, chosen via the parameter model

Add new edges to a graph, chosen uniformly at random.

Remove edges from the graph, chosen uniformly at random.

Closes open triads in a graph, according to an ego-based process.

A generalized version of Price's --- or Barabási-Albert if undirected --- preferential attachment network model.

### Generating Random Graphs


* Barabási-Albert model
* Erdős-Rényi
* Watts-Strogatz
* Random Tree
* Forest Fire Game 
* random geometric graph Graph.GRG()
* growing Graph.Growing_Random()
* establishment game Graph.Establishment()
* preference, the non-growing variant of establishment Graph.Preference()
* asymmetric preference Graph.Asymmetric_Prefernce()
* recent degree Graph.Recent_Degree()
* k-regular (each node has degree k) Graph.K_Regular()
* non-growing graph with edge probabilities proportional to node fitnesses Graph.Static_Fitness()
* non-growing graph with prescribed power-law degree distribution(s) Graph.Static_Power_Law()
* random graph with a given degree sequence Graph.Degree_Sequence()
* bipartite Graph.Random_Bipartite()


### Stochastic Block Models

Generate a random graph by sampling from the Poisson or microcanonical stochastic block model.
Generate a random graph by sampling from the maximum-entropy "canonical" stochastic block model.
Obtain SBM fugacities, given expected degrees and edge counts between groups.


### Geometric Models

Generate a graph of k-nearest neighbors (or pairs) from a set of multidimensional points.
Generate a geometric network form a set of N-dimensional points.
Generate a 2D or 3D triangulation graph from a given point set.


### Further Resources on Graph Generation

* https://github.com/yuanqidu/awesome-graph-generation
