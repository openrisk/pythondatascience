## Graph Analysis

Graph Analysis can be a vague term (used in a wide or narrow sense). Here we consider "analysis" to mean rather direct and/or transparent processing that does not involve elaborate [graph algorithms](GraphAlgorithms.md) - the focus is on the measures produced more than documenting _how_ they are produced.


### Graph Classification / Characterization 

* Bipartite
* Directed Acyclic Graph (DAG)
* Planar Graph

* Graph Characterisation
  * Trees
  * Simple
  * Directed
  * Connected


### Graph Transformations

Manipulating Graphs

Access, Add, Remove Vertices and edges

Permutations

##### Topological Sorting

a vertex property map the dominator vertices for each vertex
the topological sort of the given graph
transitive closure graph of g


Return the line graph of the given graph g.

Obtain the condensation graph, where each vertex with the same 'prop' value is condensed in one vertex.

Remove all self-loops edges from the graph.

#### Graph Operations


* union, intersection, difference, and other set operations (operators) between graphs

### Graph Analysis / Network Analysis

Find Incidence
Find Neighborhood
Compute Node Degrees

#### Global Graph Properties

* Diameter
* Girth
* Radius

#### Distributions / Moments

* Degree Distribution

vertex histogram of the given degree type or property
edge histogram of the given property
average of the given degree or vertex property
average of the given degree or vertex property
shortest-distance histogram for each vertex pair in the graph


#### Topology

* Path Length

* Shortest Distance
* Shortest Path
* Random Shortest Path
* All Circuits
* Pseudo-Diameter

#### Connectedness

#### Centrality

* PageRank
* Betweeness
* Central Point Dominance
* Closeness
* Eigevector Centrality
* Katz
* hits
* EigenTrust
* Pervasive Trust Transitivity

#### Similarity

#### Clustering

* Local Clustering Coefficients
* Global Clustering Coefficients
* Extended Clustering
* Motifs
* Motif Significance

#### Correlations

* Assortativity
* Scalar Assortativity
* Correlation Histogram
* Single-Vertex Combined Correlation Histogram

#### Cliques and Motifs



This includes the computation of various Statistics and Metrics


