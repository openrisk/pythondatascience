
## Graph Visualization / Network Visualization

This includes the computation of various Statistics and Metrics

| Functionality        | PyPI / Repo Link                                          | Description | Other  |
|----------------------|-----------------------------------------------------------| ----------- |--------|
| Explore large graphs | [pygraphstry](https://github.com/graphistry/pygraphistry) | PyGraphistry is a Python library to quickly load, shape, embed, and explore big graphs with the GPU-accelerated Graphistry visual graph analyzer   | GPU    |


graphviz - Python renderer for the DOT graph drawing language. https://github.com/xflr6/graphviz


dash-cytoscape - Interactive network visualization library in Python, powered by Cytoscape.js (Graph theory (network) library for visualization and analysis) and Dash
https://github.com/plotly/dash-cytoscape

https://github.com/palash1992/GEM GEM: Graph Embedding Methods

@article{goyal2017graph,
    title = "Graph embedding techniques, applications, and performance: A survey",
    journal = "Knowledge-Based Systems",
    year = "2018",
    issn = "0950-7051",
    doi = "https://doi.org/10.1016/j.knosys.2018.03.022",
    url = "http://www.sciencedirect.com/science/article/pii/S0950705118301540",
    author = "Palash Goyal and Emilio Ferrara",
    keywords = "Graph embedding techniques, Graph embedding applications, Python graph embedding methods GEM library"
}
@article{goyal3gem,
  title={GEM: A Python package for graph embedding methods},
  author={Goyal, Palash and Ferrara, Emilio},
  journal={Journal of Open Source Software},
  volume={3},
  number={29},
  pages={876}
}

### Graph Layout Engines

* DOT
* Neato
* FDP (Force-Directed Placement)
* Circo
* twopi (Radial)
* SFDP spring-block layout
* osage clustered graphs
* Fruchterman-Reingold spring-block layout
* ARF spring-block layout
* radial layout of the graph according to the minimum spanning tree centered at the root vertex
* canonical layout of a planar graph
* random layout of the graph

## Other

hiveplot - Python utility for drawing networks as hive plots on matplotlib, a more comprehensive network visualization

nxviz - Visualization package for NetworkX

parag - Interactive visualization of higher-order graphs in Python

webweb - Python library to produce interactive network visualizations with d3.js
