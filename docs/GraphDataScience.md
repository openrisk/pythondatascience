## Graph Data Science

Python Packages that enable Graph Data Science


### Scope and Organization

The scope aims to cover commonly defined Graph Data Science tools and algorithms.  Graphs are understood very broadly, including various types of Trees, different types of Graphs (Directed / Weighted / Labeled etc.) and Networks.

directed graphs, undirected graphs, mixed graphs, loops, multigraphs, compound graphs (a type of hypergraph), and so on.

The entries are grouped together roughly by similarity of purpose (use case) and problem context. There is substantial overlap.  Name / PyPI / Repo / Docs Links / Description | Other      

When there is no Python package the provides a certain functionality (reasonably stable / mature) the corresponding slot is empty as a placeholder.

#### Out of scope

* Machine Learning / Inference type _Graph Models_ are covered separately (even if available within the packages listed here)
* Graph Databases not in scope (Python API's to graph databases covered in the [Database Taskview](Databases.md)
* Simulation of Stochastic Dynamics on Graphs
* Calculation of Maximum / Minimum Flow Calculations on Networks
* More general visualization frameworks are here: [Visualization Taskview](Visualization.md)

### Warning

Listing a package here does not imply any sort of assurance about the quality and usability of tools, algorithms etc., the degree of testing or reliability. 

Always test packages and functionalities to a degree proportional to the significance and sensitivity of your (graph) data science task!

### Python Packages offering Graph Data Science functionality

* The first column indicates the specific method and / or functionality along with a documentation link where the _mathematics_ of the method is documented in detail (or at least futher references are provided)
* The second column has links to the package repository and/or documentation site
* The third column includes a brief description
* The third column includes and comments / remarks as appropriate


### Major Sub-Tasks

* [Graph Generation](GraphDataScience/GraphGeneration.md)
* [Graph Analysis](GraphDataScience/GraphAnalysis.md)
* [Graph Algorithms](GraphDataScience/GraphAlgorithms.md)
* [Graph Visualization](GraphDataScience/GraphVisualization.md)


### General Purpose Graph-Oriented Libraries and Tools

The following table summarizes the major _general purpose_ graph-oriented libraries in the Python ecosystem.
                                                      
#### Networkx 

* [networkx](https://github.com/networkx/networkx)
* Network Analysis in Python. A widely used framework

#### Python igraph

* [Docs](https://python.igraph.org/en/stable/index.html) 
* Python interface of igraph, a fast and open source C library to manipulate and analyze graphs (aka networks)

#### Graph-tool

* [Website](https://graph-tool.skewed.de/)
* Python module for network manipulation and analysis, written mostly in C++ for speed. Based heavily on the Boost Graph Library


### Specialized Graph Tools

* Multilayer Network Analysis [pymnet](https://github.com/mnets/pymnet)  a Python Library for Multilayer Networks. [Publication](https://joss.theoj.org/papers/10.21105/joss.06930) 
* nngt - Library-agnostic graph generation and analysis that wraps around networkx, igraph and graph-tool). Includes normalized graph measures, advanced visualizations, (geo)spatial tools, and interfaces for neuroscience simulators.
* npartite - Python algorithms for community detection in n-partite networks.
* RAPIDS cuGraph - Python packages and C/C++/CUDA libraries focused on GPU-accelerated graph analytics.
* rustworkx - A high performance Python graph library implemented in Rust.
* Snap.py - A Python interface for SNAP (a general purpose, high performance system for analysis and manipulation of large networks).
* uunet - Tools for multilayer social networks. 


#### Temporal Networks

* Raphtory - A platform for building and analysing temporal networks.
* tnetwork - Python library for temporal networks, and dynamic community detection in particular.
* TQ (Temporal Quantities) - Python 3 library for temporal network analysis.