## Semantic Data

Python Packages that support the processing of Semantic Data (RDF / OWL).

Treated separately from [Graph Data Science](GraphDataScience.md)


| Functionality | PyPI Link   | Description | Other       |
| ------------- | ----------- | ----------- | ----------- |
| Placeholder   | Placeholder | Placeholder | Placeholder |

