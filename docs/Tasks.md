# Python Data Science Tasks List


## Cross-Sectional (Horizontally Applicable Tasks)

### Mathematical / Statistical Sub-domains

#### Statistics and Probability Tools

* Probability Distributions
* [Regression](Regression.md)  
* Bayesian Inference
* [Multivariate Statistics](Statistics.md)
* Robust Statistical Methods
* [Survival Analysis](SurvivalAnalysis.md)
* Time Series Analysis
* Cluster Analysis & Finite Mixture Models
* Design of Experiments (DoE) & Analysis of Experimental Data
* Extreme Value Analysis
* Decision Analysis

#### Models, Algorithms and Simulation

* [Numerical Mathematics](Mathematics.md)
* Optimization and Linear Programming
* Differential Equations
* [Machine Learning & Statistical Learning](MachineLearning.md)
* Graphical Models


#### Data-Adapted Tools

* [Graph Data Science](GraphDataScience.md)
* [Analysis of GeoSpatial Data](Geospatial.md)
* Handling and Analyzing Spatio-Temporal Data
* Natural Language Processing
* Meta-Analysis
* Functional Data Analysis


### Workflow and Support Tools

* [Visualization](Visualization.md)
* Model Deployment
  * Native (Desktop)
  * Jupyter Notebooks
  * [Web Technologies and Services](Web.md)
* [High-Performance and Parallel Computing](HPC.md)
* [Documentation](Documentation.md)
* [Databases](Databases.md)
* Missing Data
* [Reproducible Research](Reproducibility.md)
* [Semantic Data](SemanticData.md)

## Sectoral (Domain-Specific Tasks)

### Health Sector

* Analysis of Pharmacokinetic Data
* Chemometrics and Computational Physics
* Clinical Trial Design, Monitoring, and Analysis
* Medical Image Analysis

### Biological and Environmental Sciences

* Phylogenetics, Especially Comparative Methods
* Statistical Genetics
* Analysis of Ecological and Environmental Data
* Hydrological Data and Modeling
* 
### Social Sciences & Humanities

* Statistics for the Social Sciences
* Official Statistics & Survey Methodology
* Teaching Statistics
* Psychometric Models and Methods
* 
### Economics, Finance and Insurance Sectors

* [Econometrics](Econometrics.md)
* Empirical Finance
* Sustainability Analysis


### Physical Sciences and Engineering

* Astronomy
* Atmospheric Science
* Oceanography

## PyPI Scientific/Engineering Classification Topics

* Artificial Intelligence
* Artificial Life
* Astronomy
* Atmospheric Science
* Bio-Informatics
* Chemistry
* Electronic Design Automation
* GIS
* Human Machine Interfaces
* Hydrology
* Image Processing
* Image Recognition
* Information Analysis
* Interface Engine/Protocol Translator
* Mathematics
* Medical Science
* Oceanography
* Physics
* Visualization 

## GitHub Featured Topics (Selection)

* Algorithm. Algorithms are self-contained sequences that carry out a variety of tasks.
* Code Quality / Review. Automate your code review with style, quality, security, and test‑coverage checks when you need them. Ensure your code meets quality standards and ship with confidence.
* Data Structures. Organizing and Storing Data.
* Data Visualization. Graphic representation of data and trends.
* Database. Structured Set of Data stored in a server.
* Deep Learning. Artificial NN composed of many layers.
* Machine Learning.
* Deployment.
* Documentation.
* LaTex.
* Publishing.




