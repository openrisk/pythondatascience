![Build Status](https://gitlab.com/pages/pythondatascience/badges/master/build.svg)

---

## Welcome to the Python Data Science Source Code repo

This is source code for the Python Data Science (PDS) site hosted on [GitLab Pages](https://pages.gitlab.io). 

You can [browse the PDS source code](https://gitlab.com/openrisk/pythondatascience), fork it and start contributing!


## Project Layout

Python Data Science uses MkDocs (Starting from a sample project forked from https://gitlab.com/morph027/mkdocs). 

The general repo structure is as follows:

    mkdocs.yml    # The configuration file.
    docs/
        index.md  # The main documentation homepage.
        TOPIC_1.md  # Collection of markdown pages, images and other files.
        TOPIC_2.md  # Collection of markdown pages, images and other files.
        ...
        TOPIC_N.md  # Collection of markdown pages, images and other files.


Topics that grow significantly may be split into sub-tasks as follows:

      docs/LargeTask.md  # The landing page for a Task
         LargeTask      # A subdirectory for the Task
         LargeTask/SUBTOPIC_1.md # Documents for Sub-Tasks
         LargeTask/SUBTOPIC_2.md # Documents for Sub-Tasks

---

## GitLab CI

The project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml):

```
image: python:3.8-buster

before_script:
  - pip install -r requirements.txt

test:
  stage: test
  script:
  - mkdocs build --strict --verbose --site-dir test
  artifacts:
    paths:
    - test
  except:
  - master

pages:
  stage: deploy
  script:
  - mkdocs build --strict --verbose
  artifacts:
    paths:
    - public
  only:
  - master
```

## Building Python Data Science Locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. [Install][] MkDocs
1. Preview your project: `mkdocs serve`,
   your site can be accessed under `localhost:8000`
1. Add content
1. Generate the website: `mkdocs build` (optional)

Read more at MkDocs [documentation][].


[ci]: https://about.gitlab.com/gitlab-ci/
[mkdocs]: http://www.mkdocs.org
[install]: http://www.mkdocs.org/#installation
[documentation]: http://www.mkdocs.org
[userpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#user-or-group-pages
[projpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#project-pages

---