# NACE High-level sectors

A 	NACE Section A - Agriculture, Forestry and Fishing
B 	NACE Section B - Mining and Quarrying
C 	NACE Section C - Manufacturing
D 	NACE Section D - Electricity, Gas, Steam and Air Conditioning Supply
E 	NACE Section E - Water Supply, Sewerage, Waste Management and Remediation Activities
F 	NACE Section F - Construction
G 	NACE Section G - Wholesale and Retail Trade
H 	NACE Section H - Transporting and Storage
I 	NACE Section I - Accommodation and Food Service Activities
J 	NACE Section J - Information and Communication
K 	NACE Section K - Financial and Insurance Activities
L 	NACE Section L - Real Estate Activities
M 	NACE Section M - Professional, Scientific and Technical Activities
N 	NACE Section N - Administrative and Support Service Activities
O 	NACE Section O - Public Administration and Defence, Compulsory Social Security
P 	NACE Section P - Education
Q 	NACE Section Q - Human Health and Social Work Activities
R 	NACE Section R - Arts, Entertainment and Recreation
S 	NACE Section S - Other Service Activities
T 	NACE Section T - Activities of Households as Employers
U 	NACE Section U - Activities of Extraterritorial Organisations and Bodies